<?php
/******************************************************************************
* UW LMS Usage Report - Global Settings
*
* Global (site-wide) configuration settings for LMS Usage report.
*
* Author: Nick Koeppen
******************************************************************************/
defined('MOODLE_INTERNAL') || die;

$modname = 'report_usage';

global $CFG, $SITE;

if (!$hassiteconfig) {
    return;        //Only applicable at the site level
}

/* =============== REPORT PAGES =============== */
$ADMIN->add('reports', new admin_externalpage('lmsusagereport', get_string('usagereport',$modname), "$CFG->wwwroot/report/usage/index.php"));
/* Display XML feed link only if enabled */
$feedenabled = get_config($modname, 'enablefeed');
if(!empty($feedenabled)){
    $ADMIN->add('reports' , new admin_externalpage('lmsusagefeed', get_string('usagereportfeed',$modname), "$CFG->wwwroot/report/usage/feed.php"));
}
/* =============== REPORT PAGES =============== */

$configs = array();
/* =============== SETTINGS =============== */
$defaults = array('student');
$configs[] = new admin_setting_pickroles('roles', get_string('roles',$modname), get_string('rolesconfig',$modname), $defaults);
/* Data sharing settings */
$configs[] = new admin_setting_heading('sharingtitle', get_string('sharingtitle',$modname),'');
$configs[] = new admin_setting_configtext('site', get_string('site',$modname), get_string('siteconfig',$modname), $SITE->shortname);
$configs[] = new admin_setting_configtextarea('extsites',get_string('extsites', $modname), get_string('extsitesconfig', $modname, $CFG->wwwroot), '', PARAM_TEXT);
$configs[] = new admin_setting_configcheckbox('enablefeed', get_string('enablefeed',$modname), get_string('enablefeedconfig',$modname), 0);
/* =============== SETTINGS =============== */

foreach ($configs as $config) {
   $config->plugin = $modname;
   $settings->add($config);
}
?>
<?php
$string['pluginname'] = 'LMS Usage Report';

/* Menus */
$string['usage'] = 'Usage Report';
$string['usagereport'] = 'LMS Usage';
$string['usagereportfeed'] = 'LMS Usage Feed';

/* Settings */
$string['sharingtitle'] = 'Shared Usage Data';
$string['site'] = 'Site ID';
$string['siteconfig'] = 'The unique identifying name for this Moodle site.  This name is used in Moodle Council aggregation data.';
$string['extsites'] = 'External Sites';
$string['extsitesconfig'] = 'External sites which will have their LMS usage data retrieved and displayed alongside local data.
							<br><br> Compile a CSV (Comma-separated values) list of external site roots.
							<br> - As an example this site\'s root is: <em> {$a} </em>';
$string['enablefeed'] = 'Share Site Data';
$string['enablefeedconfig'] = 'Enable to share this site\'s LMS usage data through a public XML feed.';
$string['termselection'] = 'Shared Terms';
$string['termselectionconfig'] = 'Select which terms\' LMS usage data will be shared when sharing is enabled.';
$string['termserverfail'] = 'Term data cannot be displayed. Attempt to refresh the page, otherwise check CHUB server connection.';
$string['roles'] = 'Activity Roles';
$string['rolesconfig'] = 'Users with any of these roles are checked for activity to determine whether a course can be considered active.';

/* Reports */
$string['serverfail'] = 'Timetable server connection failed';
$string['nodata'] = 'No usage data available for given parameters';
$string['select_term'] = 'Select a Term...';
$string['select_disp'] = 'Select a Display Format...';
$string['html_disp'] = 'View as HTML';
$string['excel_disp'] = 'Download to Excel';
$string['sitetitle'] = 'Site Totals';
$string['sitedefine'] = 'External Site Definitions';
$string['sitedefine_help'] = "The site information retrieved locally and from external sites defined in the settings.<br><br><a href=\"$CFG->wwwroot/admin/settings.php?section=usagesettings#admin-extsites\">Edit External Sites</a>";
$string['depttitle'] = 'Department Usage for {$a}';
$string['sites'] = 'Sites';
$string['department'] = 'Department';
$string['activesites'] = 'Activated Timetable Course Sites';
$string['courses'] = 'Unique Timetable Courses';
$string['sections'] = 'Unique Timetable Sections';
$string['enrollments'] = 'Total User Enrollments';
$string['uenrollments'] = 'Unique User Enrollments';
$string['nontimetable'] = 'Non-Timetable Sites';
$string['crosslists'] = 'Cross-listed Departments';

$string['allsitestotal'] = 'Combined Site Statistics';
$string['thissite'] = '<em> (this site) </em>';
$string['all'] = 'All Courses';
$string['nontt'] = 'Non-Timetable Sites';

/* Errors */
$string['feednotenabled'] = 'XML feed has not been enabled.';
$string['nofeedterms'] = 'No terms have been selected to display in the XML feed.';

/*  Permissions */
$string['usage:view'] = 'View LMS usage Report';
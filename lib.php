<?php
/******************************************************************************
* UW LMS Usage Report - Main Library
*
* Retrieves database information for the LMS Usage report in accordance with
* Moodle Council standards.
*
* Author: Nick Koeppen
******************************************************************************/


require_once($CFG->dirroot.'/enrol/wisc/lib/datastore.php');

function get_usage_data( $term=NULL, $dept=NULL ) {
    global $CFG, $DB;

    $config = get_config('report_usage');

    try{
    	$ds = wisc_timetable_datastore::get_timetable_datastore();

	    /* PARAMETER SETUP */
	    /* Use current semester if none given */
	    if (!isset($term)) {
	    	$term = $ds->cur_UW_Term();
	    }
	    /* Filter for departments given (all if not specified) */
	    $subs = $ds->getSubjectsInTerm($term);
	    if (!isset($dept)) {
	    	$dept = array();
	    	foreach($subs as $sub)
	    		$dept[] = $sub->subjectCode;
	    } else if (!is_array($dept)) {
	    	$dept = array($dept);
	    }
    } catch(Exception $e) {
    	return false;
    }

    /* Retrieve site statistics */
    $site_data = process_usage_data($term);

    $data = array();
    if (!empty($site_data)) {
        $data = $site_data;

        /* Retrieve specific department statistics */
    	foreach($subs as $sub){
    		if (!in_array($sub->subjectCode, $dept)) {
    			continue;
    		}
    		$sub_data = process_usage_data($term, $sub->subjectCode);
    		if ($sub_data) {
    			array_unshift($sub_data,$sub->description);
    			$data['dept'][$sub->description] = $sub_data;
    		}
    	}
    }

	return $data;
}

function process_usage_data($term, $dept = null) {
	global $DB;

	$config = get_config('report_usage');

	/* ACTIVATE COURSE SITES */
	/* Get all courses in given term and/or department */
	if (isset($dept)) {
	    $select = 'term = ? AND subject_code = ?';
	    $params = array($term, $dept);
	} else {
		$select = 'term = ?';
	    $params = array($term);
	}
    $courseids = $DB->get_fieldset_select('enrol_wisc_coursemap', 'courseid', $select, $params);
    $courseids = array_unique($courseids);

    /* Filter courses that have not been accessed according to set criteria */
	list($courseids, $allusers) = find_active_courses($courseids);

    $activesites = count($courseids);
    if($activesites == 0){
    	return;	//Skip departments with no active sites
    }

    /* UNIQUE TIMETABLE COURSES AND SECTIONS */
	$num_sections = 0;
	$sections = $DB->get_records_list("enrol_wisc_coursemap",'courseid',$courseids);
	$ttsections = array();
	foreach($sections as $section){
		/* Filter out possible multiple term sections */
	    if($section->term != $term){
	    	continue;
	    }

	    /* Total sections in active sites */
	    $ttsections[$section->subject_code][$section->catalog_number][$section->session_code] += 1;
	}

	if(isset($dept)){
		/* Count courses and sections only belonging to this subject */
		$num_courses = array_count_all($ttsections[$dept]);
		$num_sections = array_sum_all($ttsections[$dept]);
		$crosslists = count($ttsections)-1;		//All other departments

		$data = array('activesites' => $activesites,
				'courses' => $num_courses,
				'sections' => $num_sections,
		        'enrollments' => count($allusers),
		        'uenrollments'=> count(array_unique($allusers)),
				'crosslists' => $crosslists);
	}else{
		/* Count all courses and sections with the active courseids */
		$num_courses = array_count_all($ttsections);
		$num_sections = array_sum_all($ttsections);

		/* Determine nontimetable courses that are active */
		$sql = "SELECT {course}.id FROM {course}
				WHERE {course}.id NOT IN (SELECT {enrol_wisc_coursemap}.courseid FROM {enrol_wisc_coursemap})
					  AND {course}.id != ".SITEID;
		$nontt_ids = array_keys($DB->get_records_sql($sql));
		list($nontt_ids,$nontt_users) = find_active_courses($nontt_ids);
		$nontimetable = count($nontt_ids);
		/* Add all nontimetable courses to the count as well */
		$activesites += $nontimetable;

		/* Post total and nontimetable data for site */
		$all = array('value' => get_string('all','report_usage'),
				   'activesites' => $activesites,
			       'courses' => $num_courses, 'sections' => $num_sections,
			       'enrollments' => count($allusers),
			       'uenrollments'=> count(array_unique($allusers)),
			       'nontimetable' => $nontimetable);
		$nontt = array('value' => get_string('nontimetable','report_usage'),
			      'activesites' => $nontimetable, 'courses' => 'N/A', 'sections' => 'N/A',
			      'enrollments' => count($nontt_users),
			      'uenrollments'=> count(array_unique($nontt_users)),
			      'nontimetable' => $nontimetable);
		$data = array('all'=>$all,'nontt'=>$nontt);
	}

    return $data;
}

/* Filtering results */
function find_active_courses($courseids){
    global $DB;

    $config = get_config('report_usage');

    $userids = array();
    foreach($courseids as $key => $courseid){
	    /* Check that any of acceptable role users have accessed the course */
	    $roles = explode(',',$config->roles);
	    $users = array_keys(get_role_users($roles, context_course::instance($courseid)));
	    if($DB->get_records_list('user_lastaccess','userid',$users)){
		    $userids = array_merge($userids, $users);
	    }else{
	    	unset($courseids[$key]);	//Remove 'inactivate' courses
	    }
    }
    return array($courseids,$userids);
}

/* Counting functions */
function array_count_all($array) {
   //Initialized at -1 for $arr = 0
   $func = create_function("&\$arr=0,\$key=0","static \$a=-1; if(is_int(\$arr)) { \$a+=1; } return \$a;");
   array_walk_recursive($array,$func);
   return $func();
}

function array_sum_all($array) {
   $func = create_function("&\$arr=0,\$key=0","static \$a=0; if(is_int(\$arr)) { \$a+=\$arr; } return \$a;");
   array_walk_recursive($array,$func);
   return $func();
}

/* Sorting functions */
function sort_data($data,$columns,$sort,$dir){
    if(empty($data)){
        return $data;
    }
    /* Attempt to presort (data structure key) */
    if($sort == reset($columns)){
    	/* Order is reveresed since the beginning of list is the top of table */
    	if($dir == 'ASC'){
    		krsort($data);
    	}else{
    		ksort($data);
    	}
    	return $data;
    }

    /* If presorting not possible, then sort on another field */
	$orderby = array();
	foreach ($columns as $column) {
	    if ($sort != $column) {
	        array_push($orderby,$column,SORT_ASC);	// Order is reveresed since the beginning of list is the top of table
	    } else {
	        /* Order is reveresed since the beginning of list is the top of table */
	        $sortdir = $dir == 'ASC' ? SORT_DESC:SORT_ASC;
	    }
	}
	array_unshift($orderby,$sort,$sortdir); //Place sort at beginning of ordering

    return array_orderby($data,$orderby);

}
function array_orderby($data,$args){
    foreach ($args as $n => $field) {
        if (is_string($field)) {
            $tmp = array();
            foreach ($data as $key => $row)
                $tmp[$key] = $row[$field];
            $args[$n] = $tmp;
        }
    }
    $args[] = &$data;
    call_user_func_array('array_multisort', $args);
    return array_pop($args);
}

/**
 * Builds the term selector menu
 *
 * @return array consisting of $termmenu, $defaultterm, $allterms
 *
 */
function wisc_build_termmenu() {
	global $DB;

	// Any exception here is fatal, so let moodle handle it for now
	$datastore = wisc_timetable_datastore::get_timetable_datastore();
	$terms = $datastore->getAvailableTerms();

	// Grab terms that exist in the db too
	$sql = 'SELECT DISTINCT({enrol_wisc_coursemap}.term) FROM {enrol_wisc_coursemap}';
	$localtermcodes = $DB->get_records_sql($sql);
	foreach ($localtermcodes as $localtermcode){
		$localterm = $datastore->getTerm($localtermcode->term);
		if (!in_array($localterm, $terms)){
			$terms[] = $localterm;
		}
	}

	// Admins can see past terms too
	$listallterms = has_capability('moodle/site:config', get_context_instance(CONTEXT_SYSTEM));

	$futureterms = array();
	$termmenu = array();
	$now = time();
	$twomonthsago = $now - (28 * 24 * 60 * 60); // to decide default term
	foreach ($terms as $term) {
		if ($term->endDate > $now || $listallterms) {
			$termmenu[$term->termCode] = wisc_timetable_datastore::get_term_name($term->termCode);
			if ($term->termCode % 2 == 1) {
				// odd term codes are never the default (e.g. 'Winter' term)
				continue;
			}
			if ($term->beginDate > $twomonthsago && $term->endDate > $now) {
				$futureterms[] = $term->termCode;
			}
		}
	}
	if (empty($termmenu)) {
		$termmenu[0] = "No terms available";
	}

	if (!empty($futureterms)) {
		$defaultterm = min($futureterms);
	} else {
		$defaultterm = max(array_keys($termmenu));
	}

	ksort($termmenu);
	$termmenu = array_reverse($termmenu, true);

	return array($termmenu, $defaultterm, $terms);
}


function print_usage_selector($select){
	global $CFG, $PAGE, $OUTPUT;

    try{
	    list ($termmenu, $defaultterm, ) = wisc_build_termmenu();
    }catch(Exception $e){
    	echo $OUTPUT->notification(get_string('serverfail','report_usage'));
    	return;
    }

	echo '<form><fieldset>';
	/* Term chooser */
	echo html_writer::select($termmenu,'term',$select['term']);
	/* Display method chooser */
	$options = array('html','excel');
	foreach($options as $option)
		$menu[$option] = get_string($option.'_disp','report_usage');
	echo html_writer::select($menu,'disp',$select['disp']);
	/* Hidden input parameters */
	$PAGE->set_url($PAGE->url,$select);
	echo html_writer::input_hidden_params($PAGE->url,array('term','disp'));

	echo '<input type="submit" value="'.get_string("submit").'" />';
	echo '</fieldset></form>';
}

// HTML table set-up
function table_setup($clabels,$select=null,$sortprefix=''){
	global $PAGE, $OUTPUT;
	$report = 'report_usage';

	$PAGE->set_url($PAGE->url,$select);    //Add all parameters

	/* Moodle HTML table object */
	$table = new html_table();
	$table->align = array('left','center','center','center','center','center','center');
	$table->size  = array('28%','12%','12%','12%','12%','12%','12%');

	/* Column organization */
	$columns = array();
	foreach($clabels as $clabel){
		$columns[$clabel] = get_string($clabel, $report);
	}

	/* Sorting heading labels */
	$sort = $select[$sortprefix.'sort'];
	$dir = $select[$sortprefix.'dir'];
	if(isset($sort)){
		$orderby = array();
		foreach ($columns as $column=>$strcolumn) {
		    if ($sort != $column) {
		        $columndir = 'ASC';		// All other columns set to descending ('ASC' in link)
		        $columnicon = '';
		    } else {
		    	$columnicon = $dir  == 'ASC' ? 'up':'down';
		        $columndir = $dir == 'ASC' ? 'DESC':'ASC';	// Switch direction for link
		        $columnicon = " <img src=\"" . $OUTPUT->pix_url('t/' . $columnicon) . "\" alt=\"\" />";
		    }
		    $url = $PAGE->url->out(true,array($sortprefix."sort"=>$column,$sortprefix."dir"=>$columndir));
		    $table->head[] = '<a href="'.$url.'">'.$strcolumn."</a>$columnicon";
		}
	}else{
		$table->head = $columns;
	}

	return $table;
}

function table_full_row($text,$style=null){
    $row = new html_table_row();
    $cell = new html_table_cell($text);
    $cell->colspan = 7;
    if(!isset($style)){
        $cell->style = 'text-weight:bold;border-top:solid;border-bottom:solid;width:100%';
    }else{
        $cell->style = $style;
    }
    $row->cells[] = $cell;

    return $row;
}

function deptdisp_button($site,$params){
    global $PAGE, $OUTPUT;

    $params['deptdisp'] = $site;
    $PAGE->set_url($PAGE->url,$params);
    $button = new single_button($PAGE->url, 'Departments', 'get');
    $button->class = 'deptdispbutton';
    return $OUTPUT->render($button);
}

// Excel workbook set-up
function excel_setup($term){
	global $CFG, $DB;

    require_once("$CFG->libdir/excellib.class.php");
    $filename = 'LMS_usage_'.$term.'.xls';

    $workbook = new MoodleExcelWorkbook('-');
    $workbook->send($filename);

    $worksheet =& $workbook->add_worksheet($term);

    $props = array('text_wrap'=>'','bold'=>1,'align'=>'left');
    $formats['dept'] = new MoodleExcelFormat($workbook->pear_excel_workbook,$props);
    $props = array('text_wrap'=>'','align'=>'center');
    $formats['data'] = new MoodleExcelFormat($workbook->pear_excel_workbook,$props);

    return array($workbook,$worksheet,$formats);
}

function excel_columns($worksheet, $row, $headers,$formats){
    /* Configuration sizes */
    $dept_col_width = 40;
    $data_col_width = 15;
    $header_row_height = 30;

	$columns = array();
	foreach($headers as $header){
		$columns[] = get_string($header, 'report_usage');
	}

	/* Set header row height */
	$worksheet->set_row($row,$header_row_height);
	/* Treat department column different */
	$worksheet->set_column(0,0,$dept_col_width);
	$worksheet->write($row, 0, $columns[0], $formats['dept']);
	unset($columns[0]);

	/* Handle data columns */
    $col = 1;
    foreach ($columns as $column) {
    	$worksheet->set_column($row, $col, $data_col_width);
    	$worksheet->write($row, $col, $column, $formats['data']);
        $col++;
    }

    return $row + 1;
}

function excel_write_data($worksheet,$row,$data,$formats){
	foreach($data as $rowdata){
		if(is_array($rowdata)){
			$row = excel_write_row($worksheet, $row, $rowdata, $formats);
		}else{
			$row = excel_write_row($worksheet, $row, $data, $formats);
			break;
		}
	}

	return $row;
}

function excel_write_row($worksheet,$row,$rowdata,$formats){
	$col = 0;
	foreach($rowdata as $stat){
		if($col != 0){
			$format = $formats['data'];
		}else{
			$format = $formats['dept'];
		}
		$worksheet->write($row, $col, $stat, $format);
		$col++;
	}

	return $row + 1;
}

function get_external_usage_data($term=NULL){
    /* PARAMETER SETUP */
    /* Use current semester if none given */
    if(!isset($term)){
        try{
            global $CFG;
        	require_once($CFG->dirroot.'/enrol/wisc/lib/datastore.php');
        	$ds = wisc_timetable_datastore::get_timetable_datastore();
        }catch(Exception $e){
        	return false;
        }
        $term = $ds->cur_UW_Term();
    }

    $data = array();
    $errors = array();

    $config = get_config('report_usage');
    if(!empty($config->extsites)){
	    $extsites = explode(',',$config->extsites);
	    foreach($extsites as $site){
	        $feed = "$site/report/usage/feed.php?term=$term";
	        $xml = simplexml_load_file($feed);
	        if(!$xml){
	            $errors[$feed] = "No XML response";
	            continue;
	        }
	        $sitename = (string)$xml['name'];

	        if($sitedata = process_external_usage_data($xml->term)){
	            $data[$sitename] = $sitedata;
	        }
	    }
    }

    return array($data,$errors);
}

function process_external_usage_data($xml){
    /* Site totals */
    $setkeys = array('all','nontt');
    foreach($setkeys as $setkey){
        $sets[get_string($setkey,'report_usage')] = $setkey;
    }

    $data = false;
    foreach($xml->total as $total){
        $setname = (string)$total['set'];
        $row = xml2data($total,$setname);
        $data[$sets[$setname]] = $row;
    }

    /* Department data */
    foreach($xml->dept as $dept){
        $deptname = (string)$dept['name'];
        $row = xml2data($dept,$deptname);
        $data['dept'][$deptname] = $row;
    }

    return $data;
}
// Format the data into XML schema
function format_xml( $data ) {
    global $CFG;
    $config = get_config("report_usage");

    /* Handle the rest of the department data */
    $xml_data = '';
    if(!empty($data)){
        $all = array_shift($data['all']);
	    $nontt = array_shift($data['nontt']);
        $xml_data .= "<total set=\"$all\">".data2xml($data['all'])."</total>";
		$xml_data .= "<total set=\"$nontt\">".data2xml($data['nontt'])."</total>";
    	foreach($data['dept'] as $dept => $row){
	    		$dept = preg_replace('/ & /', ' AND ', $dept);
	    		$xml_data .= "<dept name=\"$dept\">".data2xml($row)."</dept>";
		}
    }

    return $xml_data;
}

function data2xml($row){
	$xml = '';

	if(!is_array($row)){
		return $xml;
	}

	foreach($row as $stat => $value){
	    if(!empty($stat)){
		    $xml .= "<$stat>$value</$stat>";
	    }
	}

	return $xml;
}

function xml2data($xml,$name){
    $row = array('value'=>$name);
    foreach((array)$xml->children() as $field => $value){
        $row[$field] = $value;
    }

    return $row;
}

function current_UW_term(){
	$now = getdate();
	if( $now['year'] >= 2000 ) {
		$century = 1;
	} else {
		$century = 0;
	}
	if( $now['mon'] >= 9 ) {
		$year = ($now['year'] + 1) % 100;
	} else {
		$year = $now['year'] % 100;
	}
	if( $year < 10 ) {
		$year = "0" . $year;
	}
	if( $now['mon'] < 6 ) {
		$term = 4;	// Jan - May = Spring
	} else if( $now['mon'] < 9 ) {
		$term = 6;	// Jun - Aug = Summer
	} else {
		$term = 2;	// Sep - Dec = Fall
	}

	return $century . $year . $term;
}
?>

<?PHP
/******************************************************************************
* UW LMS Usage Report - Version Identifier
*
* Moodle code fragment to establish plugin version.
*
* Author: Nick Koeppen
******************************************************************************/
/**
 * Version info
 *
 * @package    report
 * @subpackage usage
 * @copyright  1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die;

$plugin->version   = 2016032500;
$plugin->cron      = 0;
$plugin->component = 'report_usage';
$plugin->maturity  = MATURITY_STABLE;
$plugin->release   = 'Fall2012';
$plugin->dependencies = array(
    'enrol_wisc' => 2012021401,
);
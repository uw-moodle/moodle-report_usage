<?php
/******************************************************************************
* UW LMS Usage Report
*
* Report that generates an XML feed or html display of LMS Usage data.
*
* Author: Nick Koeppen
******************************************************************************/
require_once("../../config.php");
require_once('lib.php');

global $CFG, $DB;

/* Optional parameters */
$termcode = optional_param('term', null, PARAM_INT);

/* Check configuration settings */
$modname = 'report_usage';

$config = get_config($modname);
if (!$config->enablefeed) {
    $settingspage = "$CFG->wwwroot/$CFG->admin/settings.php?section=reportusage";
    print_error('feednotenabled', $modname, $settingspage."#admin-enablefeed");
}
/* If termcode parameter entered only return that term */
if(!isset($termcode)){
	$terms = $DB->get_fieldset_select('enrol_wisc_coursemap', 'term', '');
	$terms = array_unique($terms);
}else{
	$terms = array($termcode);
}

/* Output xml */
//Header information
header("Content-type: text/xml");
header("Content-Disposition: inline; filename=\"feed.xml\"");
header("Expires: 0");
header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
header("Pragma: public");

echo "<site name=\"$config->site\">";
foreach($terms as $term){
	echo "<term code=\"$term\">";
	echo format_xml(get_usage_data($term));
	echo "</term>";
}
echo "</site>";
?>
<?php
/**
 * Created by IntelliJ IDEA.
 * User: dsgraham
 * Date: 3/10/16
 * Time: 9:48 AM
 */

defined('MOODLE_INTERNAL') || die();

$capabilities = array(

    'report/usage:view' => array(
        'riskbitmask' => RISK_PERSONAL,
        'captype' => 'read',
        'contextlevel' => CONTEXT_SYSTEM,
        'archetypes' => array(
            'manager' => CAP_ALLOW
        ),
    )
);
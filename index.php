<?php
/******************************************************************************
* UW LMS Usage Report
*
* Report that generates an XML feed or html display of LMS Usage data.
*
* Author: Nick Koeppen
******************************************************************************/
require_once('../../config.php');
require_once($CFG->libdir.'/adminlib.php');
require_once('lib.php');

/* Context setup */
global $CFG, $PAGE, $OUTPUT;

$modname = 'report_usage';
$config = get_config($modname);

/* Ensure user is allowed to view reports */
require_capability('report/usage:view', context_system::instance());

/* Parse report parameters */
$term 	  = optional_param('term', current_UW_term(), PARAM_INT);
$deptdisp = optional_param('deptdisp', $config->site, PARAM_TEXT);
$disp 	  = optional_param('disp', 'html', PARAM_ALPHA);
$dsort    = optional_param('dsort', 'department', PARAM_ALPHA);
$ddir     = optional_param('ddir', 'DESC', PARAM_ALPHA);
$ssort    = optional_param('ssort', 'sitetotals', PARAM_ALPHA);
$sdir     = optional_param('sdir', 'DESC', PARAM_ALPHA);

$params = array('term'=>$term,'disp'=>$disp,'dsort'=>$dsort,'ddir'=>$ddir);

/* DATA RETRIEVAL */
$localdata = get_usage_data($term);
if(!empty($localdata)){
    $localdata = array($config->site => $localdata);
}
list($extdata,$errors) = get_external_usage_data($term);
/* Merge data from both local and external sources */
$data = array_merge($localdata,$extdata);

/* Separate into site and department data */
$sitedata = array();
$deptdata = array();
foreach($data as $site => $dataset){
    $sitedata[$site]['all'] = $dataset['all'];
    $sitedata[$site]['nontt'] = $dataset['nontt'];
    $deptdata[$site] = $dataset['dept'];
}

/* SITE DATA */
$scolumns = array('sites','activesites','courses','sections','enrollments','uenrollments','nontimetable');
/* Make site data sortable if there are mulitple sites */
$totalstr = get_string('allsitestotal',$modname);
if(count($sitedata) > 1){
    /* Sort on 'all' values */
    $sortedsites = array();
    foreach($sitedata as $site => $dataset){
        $sortedsites[$site] = $dataset['all'];
    }
    $sortedsites = sort_data($sortedsites,$scolumns,$ssort,$sdir);
    $sorted = array();
    foreach($sortedsites as $site => $dataset){
        $sorted[$site] = $sitedata[$site];
    }
    $sitedata = $sorted;

    /* Add total data set to top */
    $total = array();
    foreach($sitedata as $dataset){
        foreach($dataset as $set => $row){
            $total[$set]['value'] = $row['value'];
            unset($row['value']);
            foreach($row as $field => $value){
                $total[$set][$field] += $value;
            }
        }
    }
    $sitedata = array_merge(array($totalstr=>$total),$sitedata);
    /* Include site sorting and different department disp parameters */
    $params['deptdisp'] = $deptdisp;
    $params['ssort'] = $ssort;
    $params['sdir'] = $sdir;
}

$PAGE->set_url(new moodle_url('/report/usage/index.php', $params));

/* DEPARTMENTAL DATA */
$dcolumns = array('department','activesites','courses','sections','enrollments','uenrollments','crosslists');
if($deptdisp == $totalstr){
    $total = array();
    foreach($deptdata as $dataset){
        foreach($dataset as $set => $row){
            $total[$set]['value'] = array_shift($row);
            foreach($row as $field => $value){
                $total[$set][$field] += $value;
            }
        }
    }
    $deptdata[$totalstr] = $total;    //Display combined statistics
}
if (!empty($deptdata))
	$deptdata = sort_data($deptdata[$deptdisp],$dcolumns,$dsort,$ddir);

/* DATA DISPLAY */
if($disp == 'html' || empty($data)){
    admin_externalpage_setup('lmsusagereport', '', null, '', array('pagelayout'=>'report'));
	echo $OUTPUT->header();
	echo $OUTPUT->heading(get_string('usagereport', $modname));

	print_usage_selector($params);

	if(!empty($errors)){
	    foreach($errors as $feed => $error){
	        echo $OUTPUT->notification("Error with feed ($feed): $error");
	    }
	}

	if(empty($sitedata)){
		echo get_string('nodata',$modname);
		echo $OUTPUT->footer();
		return;
	}else{
	    echo $OUTPUT->heading_with_help(get_string('sitetitle', $modname),'sitedefine',$modname);
	    /* Determine whether to have sortable data */
	    if(count($sitedata) == 1){
	        $table = table_setup($scolumns);
	    }else{
	        $table = table_setup($scolumns,$params,'s');
	    }
	    /* Output site data into table */
        foreach($sitedata as $site => $dataset){
            $button = $deptdisp != $site ? deptdisp_button($site,$params) : '';
            if($site == $config->site){
                $site = $config->site.' '.get_string('thissite',$modname);
            }
            $table->data[] = table_full_row($site.$button);
            $table->data[] = $dataset['all'];
	        $table->data[] = $dataset['nontt'];
        }
	    /* Local site should always be displayed to avoid confusion */
	    if(empty($localdata)){
	        $table->data[] = table_full_row($config->site.' '.get_string('thissite',$modname));
	        $table->data[] = table_full_row(get_string('nodata',$modname),'text-align:center;width:100%');
	    }
		echo html_writer::table($table);
	}

	if($deptdata){
    	echo $OUTPUT->heading(get_string('depttitle', $modname,$deptdisp));

    	$table = table_setup($dcolumns,$params,'d');
    	$table->data = $deptdata;
    	echo html_writer::table($table);
	}

	echo $OUTPUT->footer();
}elseif($disp == 'excel'){
	/* Set up excel workbook */
	list($workbook, $worksheet,$formats) = excel_setup($term);
	/* Place columns for data */
	$row = 0;
	$row = excel_columns($worksheet,$row,$scolumns,$formats);
	foreach($sitedata as $site => $rowdata){
	    $row = excel_write_row($worksheet, $row, array($site), $formats);
	    $row = excel_write_data($worksheet, $row, $rowdata, $formats);
	    $row++;    //Separate sites' data
	}
	$row++; //Spacer between site and departmental data

	$row = excel_columns($worksheet,$row,$dcolumns,$formats);
	excel_write_data($worksheet, $row, $deptdata, $formats);
	$workbook->close();
}else{
	return;
}
?>